
import { repository } from '@loopback/repository';
import { CharactersRepository, BooksRepository, HousesRepository } from '../repositories';
var request = require('request');

export class MigrateData {
    constructor(
        @repository(BooksRepository)
        public booksRepository: BooksRepository,
        @repository(HousesRepository)
        public housesRepository: HousesRepository,
        @repository(CharactersRepository)
        public charactersRepository: CharactersRepository,

    ) { }

    async CreateData(data: any, type: any): Promise<any[]> {
        let parseData = JSON.parse(data)
        let result = []

        let modelOptions = this.booksRepository;

        switch (type) {
            case 'character':
                modelOptions = this.charactersRepository
                break;

            case 'book':
                modelOptions = this.booksRepository
                break;

            case 'house':
                modelOptions = this.housesRepository
                break;
        }

        try {
            for await (let item of parseData) {
                const [isExist] = await modelOptions.find({
                    where: {
                        url: item.url
                    }
                })
             
                if (!isExist) {
                    const newData = await modelOptions.create(item)
                    result.push(newData)
                }
            }

        } catch (e) {
            console.log('error =>', e)
        }

        return result
    }

}
