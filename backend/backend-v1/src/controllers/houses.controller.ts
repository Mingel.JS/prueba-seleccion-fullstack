import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Houses} from '../models';
import {HousesRepository} from '../repositories';

export class HousesController {
  constructor(
    @repository(HousesRepository)
    public housesRepository : HousesRepository,
  ) {}

  @post('/houses')
  @response(200, {
    description: 'Houses model instance',
    content: {'application/json': {schema: getModelSchemaRef(Houses)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Houses, {
            title: 'NewHouses',
            exclude: ['id'],
          }),
        },
      },
    })
    houses: Omit<Houses, 'id'>,
  ): Promise<Houses> {
    return this.housesRepository.create(houses);
  }

  @get('/houses/count')
  @response(200, {
    description: 'Houses model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Houses) where?: Where<Houses>,
  ): Promise<Count> {
    return this.housesRepository.count(where);
  }

  @get('/houses')
  @response(200, {
    description: 'Array of Houses model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Houses, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Houses) filter?: Filter<Houses>,
  ): Promise<Houses[]> {
    return this.housesRepository.find(filter);
  }

  @patch('/houses')
  @response(200, {
    description: 'Houses PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Houses, {partial: true}),
        },
      },
    })
    houses: Houses,
    @param.where(Houses) where?: Where<Houses>,
  ): Promise<Count> {
    return this.housesRepository.updateAll(houses, where);
  }

  @get('/houses/{id}')
  @response(200, {
    description: 'Houses model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Houses, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Houses, {exclude: 'where'}) filter?: FilterExcludingWhere<Houses>
  ): Promise<Houses> {
    return this.housesRepository.findById(id, filter);
  }

  @patch('/houses/{id}')
  @response(204, {
    description: 'Houses PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Houses, {partial: true}),
        },
      },
    })
    houses: Houses,
  ): Promise<void> {
    await this.housesRepository.updateById(id, houses);
  }

  @put('/houses/{id}')
  @response(204, {
    description: 'Houses PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() houses: Houses,
  ): Promise<void> {
    await this.housesRepository.replaceById(id, houses);
  }

  @del('/houses/{id}')
  @response(204, {
    description: 'Houses DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.housesRepository.deleteById(id);
  }
}
