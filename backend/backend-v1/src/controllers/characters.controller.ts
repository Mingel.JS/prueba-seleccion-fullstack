import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import { Characters } from '../models';
import { CharactersRepository, BooksRepository, HousesRepository } from '../repositories';
import { MigrateData } from '../scripts/migrate_data'
var request = require('request');

export class CharactersController {

  migrateCharacters: MigrateData;
  constructor(
    @repository(BooksRepository)
    public booksRepository: BooksRepository,
    @repository(HousesRepository)
    public housesRepository: HousesRepository,
    @repository(CharactersRepository)
    public charactersRepository: CharactersRepository
  ) {
    this.migrateCharacters = new MigrateData(this.booksRepository, this.housesRepository, this.charactersRepository)
  }

  @post('/characters')
  @response(200, {
    description: 'Characters model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Characters) } },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Characters, {
            title: 'NewCharacters',
            exclude: ['id'],
          }),
        },
      },
    })
    characters: Omit<Characters, 'id'>,
  ): Promise<Characters> {
    return this.charactersRepository.create(characters);
  }

  @get('/characters/count')
  @response(200, {
    description: 'Characters model count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async count(
    @param.where(Characters) where?: Where<Characters>,
  ): Promise<Count> {
    return this.charactersRepository.count(where);
  }

  @get('/characters')
  @response(200, {
    description: 'Array of Characters model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Characters, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.filter(Characters) filter?: Filter<Characters>,
  ): Promise<Characters[]> {
    return this.charactersRepository.find(filter);
  }

  @patch('/characters')
  @response(200, {
    description: 'Characters PATCH success count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Characters, { partial: true }),
        },
      },
    })
    characters: Characters,
    @param.where(Characters) where?: Where<Characters>,
  ): Promise<Count> {
    return this.charactersRepository.updateAll(characters, where);
  }

  @get('/characters/{id}')
  @response(200, {
    description: 'Characters model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Characters, { includeRelations: true }),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Characters, { exclude: 'where' }) filter?: FilterExcludingWhere<Characters>
  ): Promise<Characters> {
    return this.charactersRepository.findById(id, filter);
  }

  @patch('/characters/{id}')
  @response(204, {
    description: 'Characters PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Characters, { partial: true }),
        },
      },
    })
    characters: Characters,
  ): Promise<void> {
    await this.charactersRepository.updateById(id, characters);
  }

  @put('/characters/{id}')
  @response(204, {
    description: 'Characters PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() characters: Characters,
  ): Promise<void> {
    await this.charactersRepository.replaceById(id, characters);
  }

  @del('/characters/{id}')
  @response(204, {
    description: 'Characters DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.charactersRepository.deleteById(id);
  }

  @get('/characters/initialize')
  @response(204, {
    description: 'Characters migrate success',
  })
  async getCharacters(): Promise<void> {
    let result

    const saveCharacters = async (error: any, response: any, body: any) => {
      result = await this.migrateCharacters.CreateData(body, 'character')
    }

    const saveBooks = async (error: any, response: any, body: any) => {
      await this.migrateCharacters.CreateData(body, 'book')
    }

    const saveHouses = async (error: any, response: any, body: any) => {
      await this.migrateCharacters.CreateData(body, 'house')
    }

    for (var i = 1; i < 10;) {
      await request(`https://www.anapioficeandfire.com/api/characters?page=${i}&pageSize=50`, saveCharacters)
      i++
    }

    for (var i = 1; i < 45;) {
      await request(`https://www.anapioficeandfire.com/api/books?page=${i}&pageSize=50`, saveBooks)
      i++
    }

    for (var i = 1; i < 45;) {
      await request(`https://www.anapioficeandfire.com/api/houses?page=${i}&pageSize=50`, saveHouses)
      i++
    }


    return result
  }
}

