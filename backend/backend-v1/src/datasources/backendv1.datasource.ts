import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'backendv1',
  connector: 'postgresql',
  url: '',
  host: 'localhost',
  port: 5432,
  user: 'postgres',
  password: '123',
  database: 'backend-v1'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class Backendv1DataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'backendv1';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.backendv1', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
