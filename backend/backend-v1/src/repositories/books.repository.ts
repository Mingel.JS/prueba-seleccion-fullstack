import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {Backendv1DataSource} from '../datasources';
import {Books, BooksRelations} from '../models';

export class BooksRepository extends DefaultCrudRepository<
  Books,
  typeof Books.prototype.id,
  BooksRelations
> {
  constructor(
    @inject('datasources.backendv1') dataSource: Backendv1DataSource,
  ) {
    super(Books, dataSource);
  }
}
