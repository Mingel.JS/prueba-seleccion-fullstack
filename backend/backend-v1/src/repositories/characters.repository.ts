import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {Backendv1DataSource} from '../datasources';
import {Characters, CharactersRelations} from '../models';

export class CharactersRepository extends DefaultCrudRepository<
  Characters,
  typeof Characters.prototype.id,
  CharactersRelations
> {
  constructor(
    @inject('datasources.backendv1') dataSource: Backendv1DataSource,
  ) {
    super(Characters, dataSource);
  }
}
