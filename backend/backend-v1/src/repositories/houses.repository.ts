import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {Backendv1DataSource} from '../datasources';
import {Houses, HousesRelations} from '../models';

export class HousesRepository extends DefaultCrudRepository<
  Houses,
  typeof Houses.prototype.id,
  HousesRelations
> {
  constructor(
    @inject('datasources.backendv1') dataSource: Backendv1DataSource,
  ) {
    super(Houses, dataSource);
  }
}
