import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Characters extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  url?: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  gender?: string;

  @property({
    type: 'string'
  })
  culture?: string;

  @property({
    type: 'string',
  })
  born?: string;

  @property({
    type: 'string',
  })
  died?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  aliases?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  titles?: string[];

  @property({
    type: 'string',
  })
  father?: string;

  @property({
    type: 'string',
  })
  mother?: string;

  @property({
    type: 'string',
  })
  spouse?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  allegiances?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  books?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  povBooks?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  tvSeries?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  playedBy?: string[];

 

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Characters>) {
    super(data);
  }
}

export interface CharactersRelations {
  // describe navigational properties here
}

export type CharactersWithRelations = Characters & CharactersRelations;
