import {Entity, model, property} from '@loopback/repository';

@model({settings: {}})
export class Books extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  url?: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string'
  })
  isbn?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  authors?: string[];

  @property({
    type: 'number',
  })
  numberOfPages?: number;

  @property({
    type: 'string',
  })
  publisher?: string;

  @property({
    type: 'string',
  })
  country?: string;

  @property({
    type: 'string',
  })
  mediaType?: string;

  @property({
    type: 'date',
  })
  released?: string;

  @property({
    type: 'array',
    itemType: 'string',
  })
  characters?: string[];

  @property({
    type: 'array',
    itemType: 'string',
  })
  povCharacters?: string[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Books>) {
    super(data);
  }
}

export interface BooksRelations {
  // describe navigational properties here
}

export type BooksWithRelations = Books & BooksRelations;
