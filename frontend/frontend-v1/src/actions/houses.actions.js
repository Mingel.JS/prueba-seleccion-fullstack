import axios from 'axios'

export const LOAD_HOUSES = '[Houses] LOAD_HOUSES';
export function loadHouses(){
  return { type: LOAD_HOUSES }
}

export const LOAD_HOUSES_SUCCESS = '[Houses] LOAD_HOUSES_SUCCESS';
export function loadSuccess(houses){
  return { type: LOAD_HOUSES_SUCCESS, houses }
}

export const LOAD_HOUSES_FAILED = '[Houses] LOAD_HOUSES_FAILED';
export function loadFailed(error){
  return { type: LOAD_HOUSES_FAILED, error }
}

export function fetchHouses(filters = {}){
    return async function(dispatch, getState){
      dispatch(loadHouses())
  
      try{
        let url = `${window.config.API_URL}houses`
  
        const res = await axios.get(url, {
          headers: {}
        });
  
        if(res.status === 200){
          dispatch(loadSuccess(res.data))
        }else{
          dispatch(loadFailed(res))
        }
      }catch(err){
        dispatch(loadFailed(err))
      }
    }
  }