import axios from 'axios'

export const LOAD_BOOKS = '[Books] LOAD_BOOKS';
export function loadBooks(){
  return { type: LOAD_BOOKS }
}

export const LOAD_BOOKS_SUCCESS = '[Books] LOAD_BOOKS_SUCCESS';
export function loadSuccess(books){
  return { type: LOAD_BOOKS_SUCCESS, books }
}

export const LOAD_BOOKS_FAILED = '[Books] LOAD_BOOKS_FAILED';
export function loadFailed(error){
  return { type: LOAD_BOOKS_FAILED, error }
}

export function fetchBooks(filters = {}){
    return async function(dispatch){
      dispatch(loadBooks())
  
      try{
        let url = `${window.config.API_URL}books`
  
        const res = await axios.get(url, {
          headers: {}
        });
  
        if(res.status === 200){
          dispatch(loadSuccess(res.data))
        }else{
          dispatch(loadFailed(res))
        }
      }catch(err){
        dispatch(loadFailed(err))
      }
    }
  }