import axios from 'axios'

export const LOAD_CHARACTERS = '[Characters] LOAD_CHARACTERS';
export function loadCharacters(){
  return { type: LOAD_CHARACTERS }
}

export const LOAD_CHARACTERS_SUCCESS = '[Characters] LOAD_CHARACTERS_SUCCESS';
export function loadCharactersSuccess(characters){
  return { type: LOAD_CHARACTERS_SUCCESS, characters }
}

export const LOAD_CHARACTERS_FAILED = '[Characters] LOAD_CHARACTERS_FAILED';
export function loadCharactersFailed(error){
  return { type: LOAD_CHARACTERS_FAILED, error }
}

export const INITIALIZE = '[Data] INITIALIZE';
export function initializeData(){
  return { type: INITIALIZE }
}

export const INITIALIZE_SUCCESS = '[Data] INITIALIZE_SUCCESS';
export function initializeSuccess(){
  return { type: INITIALIZE_SUCCESS }
}

export const INITIALIZE_FAIL = '[Data] INITIALIZE_FAIL';
export function initializeFail(){
  return { type: INITIALIZE_FAIL }
}



export function fetchCharacters(filters = {}){
    return async function(dispatch, getState){
      dispatch(loadCharacters())
  
      try{
        let url = `${window.config.API_URL}characters`
  
        // if( Object.keys(filters).length ){
          // url += '?' + toQueryString({page: 1})
        // }
  
        const res = await axios.get(url, {
          headers: {}
        });
  
        if(res.status === 200){
        //   dispatch(updatePagination(res.headers)) 
          dispatch(loadCharactersSuccess(res.data))
        }else{
          dispatch(loadCharactersFailed(res))
        }
      }catch(err){
        dispatch(loadCharactersFailed(err))
      }
    }
  }

  export function loadCharacterById(CharacterId){
    return async function(dispatch, getState){
      dispatch(loadCharacters())
  
      try{
        const temporalData = []
        let url = `${window.config.API_URL}characters/${CharacterId}`
  
        const res = await axios.get(url, {
          headers: {}
        });
  
        if(res.status === 200){
          temporalData.push(res.data)
          dispatch(loadCharactersSuccess(temporalData))
        }else{
          dispatch(loadCharactersFailed(res))
        }
      }catch(error){
        dispatch(loadCharactersFailed(error))
      }
    }
  }

  export function initialize(){
    return async function(dispatch, getState){
      dispatch(loadCharacters())
  
      try{
        let url = `${window.config.API_URL}characters/initialize`
  
        const res = await axios.get(url, {
          headers: {}
        });
     

        if(res.status === 200){
          dispatch(loadCharactersSuccess(res.data))
        }else{
          dispatch(loadCharactersFailed(res))
        }
      }catch(err){
        dispatch(loadCharactersFailed(err))
      }
    }
  }