import * as HouseActions from '../actions/houses.actions'

const INITIAL_STATE = {
    entities: {},
    ids: [],
    isLoading: false,
    loadingById: {}
}

export default function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
        case HouseActions.LOAD_HOUSES:
            return {
                ...state,
                isLoading: true
            }

        case HouseActions.LOAD_HOUSES_SUCCESS:
            return {
                ...state,
                isLoading: false,
                entities: action.houses.reduce((acc, item) => {
                    acc[item.id] = item
                    return acc
                }, {}),
                ids: action.houses.map(x => x.id)
            }

        default:
            return state;
    }
}

export const getHouseEntities = state => state.houses.entities
export const getHouses = state => state.houses.ids.map(id => state.houses.entities[id])
export const getById = (state, HouseId) => state.houses.entities[HouseId]

export const getIsLoading = state => state.houses.isLoading
export const getIsLoadingById = (state, HouseId) => {
    if (HouseId === undefined) {
        return false
    }

    return state.houses.loadingById[HouseId]
}

