import * as BookActions from '../actions/books.actions'

const INITIAL_STATE = {
    entities: {},
    ids: [],
    isLoading: false,
    loadingById: {}
}

export default function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
        case BookActions.LOAD_BOOKS:
            return {
                ...state,
                isLoading: true
            }

        case BookActions.LOAD_BOOKS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                entities: action.books.reduce((acc, item) => {
                    acc[item.id] = item
                    return acc
                }, {}),
                ids: action.books.map(x => x.id)
            }

        default:
            return state;
    }
}

export const getHouseEntities = state => state.books.entities
export const getBooks = state => state.books.ids.map(id => state.books.entities[id])
export const getById = (state, BookId) => state.books.entities[BookId]

export const getIsLoading = state => state.books.isLoading
export const getIsLoadingById = (state, BookId) => {
    if (BookId === undefined) {
        return false
    }

    return state.books.loadingById[BookId]
}

