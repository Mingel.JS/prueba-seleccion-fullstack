import { combineReducers } from 'redux'
import characters from './characters.reducer.js'
import books from './books.reducer'
import houses from './houses.reducer'
const reducers = combineReducers({
    characters,
    books,
    houses
  });
  
  export default reducers;