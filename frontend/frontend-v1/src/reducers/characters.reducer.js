import * as CharacterActions from '../actions/characters.actions'
import { getBooks } from './books.reducer'
import { getHouses } from './houses.reducer'
import { createSelector } from 'reselect'

const INITIAL_STATE = {
    entities: {},
    ids: [],
    isLoading: false,
    loadingById: {},
    pagination: {
        page: 1,
        limit: 10,
        totalItems: 0,
        totalPages: 0
    }
}

export default function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
        case CharacterActions.LOAD_CHARACTERS:
        case CharacterActions.INITIALIZE:
            return {
                ...state,
                isLoading: true
            }

        case CharacterActions.LOAD_CHARACTERS_SUCCESS:
        case CharacterActions.INITIALIZE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                entities: action.characters.reduce((acc, item) => {
                    acc[item.id] = item
                    return acc
                }, {}),
                ids: action.characters.map(x => x.id)
            }

        // case CharacterActions.UPDATE_PAGINATION:
        //   return {
        //     ...state,
        //     pagination: {
        //       ...state.pagination,
        //       page: parseInt(action.headers['x-page']),
        //       limit: parseInt(action.headers['x-limit']),
        //       totalItems: parseInt(action.headers['x-total-items']),
        //       totalPages: parseInt(action.headers['x-total-pages'])
        //     }
        //   }



        default:
            return state;
    }
}

export const getCharacterEntities = state => state.characters.entities
export const getCharacters = state => state.characters.ids.map(id => state.characters.entities[id])
export const getCharacterById = (state, CharacterId) => state.characters.entities[CharacterId]
export const getPagination = state => state.characters.pagination


export const getIsLoading = state => state.characters.isLoading
export const getIsLoadingById = (state, CharacterId) => {
    if (CharacterId === undefined) {
        return false
    }

    return state.characters.loadingById[CharacterId]
}


export const getData = state =>
    createSelector(
        getCharacters,
        getBooks,
        getHouses,
        (characters, books, houses) => {
            if (characters.length && books.length && houses.length) {

                characters.map((item) => {
                    item.house = houses.find(house =>
                        house.currentLord === item.url ||
                        house.swornMembers.includes(item.url) ||
                        house.founder === item.url)

                    item.books = books.filter(book =>
                        book.characters.includes(item.url) ||
                        book.povCharacters.includes(item.url)
                    )
                })

            }

            return characters
        }
    )(state)

export const getDataById = (state, id) =>
    createSelector(
        getCharacters,
        getBooks,
        getHouses,
        (characters, books, houses) => {
            let current = {}
            if (characters.length && books.length && houses.length) {
                current = characters.find(item => item.id === parseInt(id))

                current.house = houses.find(house =>
                    house.currentLord === current.url ||
                    house.swornMembers.includes(current.url) ||
                    house.founder === current.url)

                current.books = books.filter(book =>
                    book.characters.includes(current.url) ||
                    book.povCharacters.includes(current.url)
                )

            }
            return current
        }
    )(state)

