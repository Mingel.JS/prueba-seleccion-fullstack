import logo from './logo.svg';
import './App.css';

import React, { useEffect, useState } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import history from "./history.js";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";

import CharacterPage from "./app/characters/CharacterPage";
import CharacterData from "./app/characters/CharacterData";
import HomePage from "./app/characters/Home";
function App() {
  return (
    <Container fluid={true} className="p-0">
      <Router history={history}>
        <Col>
          <Switch>
            <Route path="/" exact>
              <Redirect to="/home" />
            </Route>

            <Route path="/home" exact component={HomePage} />
            <Route path="/list" exact component={CharacterPage} />
            <Route path="/list/:CharacterId" exact component={CharacterData} />
            <Switch>

              <Route path="*">
                <Redirect to="/home" />
              </Route>
            </Switch>

          </Switch>
        </Col>
      </Router>
    </Container>

  );
}

export default App;
