import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import 'bootstrap/dist/css/bootstrap.css';
import './assets/style.css'

import { createStore, compose, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import reducers from './reducers/index.js';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
let IS_RENDERED = false;

function render() {
  if (IS_RENDERED) {
    return;
  }


  IS_RENDERED = true;

  const store = createStore(
    reducers,
    composeEnhancers(
      applyMiddleware(
        thunkMiddleware,
      ),
    )
  );

  ReactDOM.render(
    <Provider store={store}>
      <App store={store} />
    </Provider>,
    document.getElementById('root')
  );

}


setTimeout(render, 50)
