import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';


import * as CharacterActions from '../../actions/characters.actions'

function HomePage() {
    const dispatch = useDispatch()
    const history = useHistory()


    const initialize = () => {
        dispatch(CharacterActions.initialize())
    }
    return (
        <Container fluid={true} className="my-3 containerPage">
            <Row className='d-flex justify-content-center'>
                <Col lg={5} className='d-flex justify-content-between'>
                    <button className="btn btn-info"
                        onClick={() => history.push(`/list`)}>
                        Lista de personajes
                    </button>
                    <button
                        className="btn btn-warning"
                        onClick={() => initialize()}>
                        Migrar datos
                    </button>
                </Col>
            </Row>
            <Row>

            </Row>

        </Container >
    );
}

export default HomePage;
