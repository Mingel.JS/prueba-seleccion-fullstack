import React from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


const Pagination = ({ perPage, totalCharacters, paginate }) => {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalCharacters / perPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <nav>
            <ul className='pagination'>
                {pageNumbers.map(number => (
                    <li key={number} className='page-item'>
                        <a onClick={() => paginate(number)} className='page-link'>
                            {number}
                        </a>
                    </li>
                ))}
            </ul>
        </nav >

    );
};

export default Pagination;