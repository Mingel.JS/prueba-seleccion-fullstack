import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'

import * as CharacterActions from '../../actions/characters.actions'
import * as CharacterReducer from '../../reducers/characters.reducer'

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Spinner from '../spinner/Spinner.js';
import history from '../../history.js'

// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faUser, faTrash } from '@fortawesome/free-solid-svg-icons';

import Table from 'react-bootstrap/Table';





export default function CharacterTable({ characters, tableSize, input }) {
    // let characters = useSelector(CharacterReducer.getData)
    const isLoading = useSelector(CharacterReducer.getIsLoading)
    const dispatch = useDispatch()
    const [currentCharactes, setCurrentCharacters] = useState(characters)


    useEffect(() => {
        setCurrentCharacters(characters.filter(x => x !== undefined))
    }, [characters, characters.length])


    // if (isLoading && characters.length === 0) {
    //     return (
    //         <div className="container-lg py-4 p-0 text-center">
    //             <Spinner />
    //         </div>
    //     )
    // }

    if (characters.length === 0 && !isLoading) {
        return <div className="not-found-table-items">No se encontraron personajes</div>
    }

    return (
        <Table
            striped
            bordered
            hover
            className={`mb-0 ${tableSize ? 'table-sm' : ''}`}
        >
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Casa</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {
                    currentCharactes.map((item) => {
                        return (
                            <tr key={item.id}>
                                <td>
                                    {/* <Link to={"/users/" + item.id + "/edit"}>{item.name}</Link> */}
                                    {item.name}
                                </td>
                                <td>
                                    {item.house ? item.house.name : 'Ninguna'}
                                </td>
                                <td className='d-flex justify-content-center'>
                                    <button
                                        onClick={() => history.push(`/list/${item.id}`)}
                                        className='btn btn-success mr-2'>
                                        Ver Datos
                                    </button>


                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </Table>
    )
}

