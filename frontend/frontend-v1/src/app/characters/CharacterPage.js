import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

// import { usePagination } from '../shared/Pagination.js'
// import useSearch from '../shared/Search.js'

import CharacterTable from './CharacterTable'
import Pagination from './Pagination'
import * as CharacterActions from '../../actions/characters.actions'
import * as BookActions from '../../actions/books.actions'
import * as HouseActions from '../../actions/houses.actions'

import * as CharacterReducer from '../../reducers/characters.reducer'
import * as HouseReducer from '../../reducers/houses.reducer'

const SearchBar = ({ keyword, setKeyword }) => {
    const BarStyling = { width: "20rem", background: "#F2F1F9", border: "none", padding: "0.5rem" };
    return (
        <input
            style={BarStyling}
            key="random1"
            value={keyword}
            placeholder={"Buscar personaje..."}
            onChange={(e) => setKeyword(e.target.value)}
        />
    );
}

function CharacterPage() {
    const characters = useSelector(CharacterReducer.getData)
    const houses = useSelector(HouseReducer.getHouses)
    const dispatch = useDispatch()
    const history = useHistory()
    const [input, setInput] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [perPage] = useState(10);
    const [currentCharactes, setCurrentCharacters] = useState(characters)
    const [filterCharacters, setFilterCharacters] = useState([])



    const updateSearch = async (input) => {
        setInput(input)
    }

    useEffect(() => {
        dispatch(CharacterActions.fetchCharacters())
        dispatch(BookActions.fetchBooks())
        dispatch(HouseActions.fetchHouses())
    }, [dispatch, history])


    useEffect(() => {
        let filterCharacters
        if (input) {
            let filtered = characters.filter(item =>
                item.name.toLowerCase().includes(input.toLowerCase())
            )
            filterCharacters = filtered.filter(x => x !== undefined)
            paginate(1)
        } else {
            filterCharacters = characters.filter(x => x !== undefined)
        }
       

        const lastCharacter = currentPage * perPage
        const firstCharacter = lastCharacter - perPage
        setCurrentCharacters(filterCharacters.slice(firstCharacter, lastCharacter))
    }, [input, currentPage, characters, houses])



    const paginate = (pageNumber) => setCurrentPage(pageNumber)

    return (
        <Container fluid={true} className="my-3 containerPage">
            <Row>
                <Col>
                    <SearchBar
                        input={input}
                        setKeyword={updateSearch}
                    />
                </Col>
            </Row>
            <Row>
                <Col className="pt-2 pr-0 pb-0 pl-0">
                    <Card>
                        <Card.Body className="p-0 table-responsive">
                            <CharacterTable
                                characters={currentCharactes}
                                input={input} />
                        </Card.Body>
                    </Card>

                </Col>

            </Row>
            <Row className="pt-3">
                <Col lg={10}>
                    {
                        characters.length > 0  &&
                        <Pagination perPage={perPage} totalCharacters={characters.length} paginate={paginate} />
                    }
                </Col>
            </Row>

        </Container>
    );
}

export default CharacterPage;
