import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import { useParams } from 'react-router';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

import * as CharacterReducer from '../../reducers/characters.reducer'
import * as CharacterActions from '../../actions/characters.actions'
import * as BookActions from '../../actions/books.actions'
import * as HouseActions from '../../actions/houses.actions'

export default function CharacterData() {
    const dispatch = useDispatch()
    const history = useHistory()
    const params = useParams()

    useEffect(() => {
        dispatch(CharacterActions.loadCharacterById(params.CharacterId))
        dispatch(BookActions.fetchBooks())
        dispatch(HouseActions.fetchHouses())
    }, [dispatch, params.CharacterId])


    let currentCharacter = useSelector(state => CharacterReducer.getDataById(state, params.CharacterId))
    
    return (
        <Container fluid >
            <Row className="d-flex justify-content-center pt-5">
                <Col lg={9}>
                    <Card>
                        <Card.Header className="d-flex justify-content-center">
                            <span>{currentCharacter.name}</span>
                        </Card.Header>
                        <Card.Body  className="d-flex justify-content-center">
                            <Row>
                                <Col>
                                    <span>Sexo: {currentCharacter.gender}</span> <br />
                                    <span>Casa: {currentCharacter.house ?
                                        currentCharacter.house.name : 'Ninguna'}</span> <br />
                                    <span>
                                        Libros:
                                        {
                                            currentCharacter.books &&
                                                currentCharacter.books.length > 0 ?
                                                currentCharacter.books.map(x =>
                                                    <div key={x.url}><span >{x.name}</span> <br /> </div>
                                                ) : 'Ninguno'


                                        }
                                    </span>
                                </Col>
                                <Col>
                                <span>Nacimiento: {currentCharacter.born}</span> <br />
                                    <span>
                                        Titulos: 
                                        {
                                            currentCharacter.titles &&
                                                currentCharacter.titles.length > 0 ?
                                                currentCharacter.titles.map(x =>
                                                    <div key={x}><span >{x}</span> <br /> </div>
                                                ) : 'Ninguno'


                                        }
                                    </span>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}